#!/usr/bin/env python

import click
from collections import OrderedDict
from data_spaces import DataManager
import datetime
import json
from pathlib import Path
import logging
import os
import re
import sys
from yaml import load as load_yaml, dump as dump_yaml

# try to use libyaml, then fall back to pure python if not available
try:
    # we're using C/BaseLoader to ensure all values are strings as expected
    from yaml import CBaseLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import BaseLoader as Loader, Dumper

CLICK_SETTINGS = dict(help_option_names=["-h", "--help"])
VALID_PLATFORMS = (
    "all",
    "fake-sensitive-db",
    "goldstandard",
    "target",
    "wes-hiseq",
    "wes-novaseq",
    "wgs-hiseq",
    "wgs-novaseq",
)
VALID_TYPES = ("all", "analyses", "samples", "results")
MGR_OPTS = {}
TOUCHFILE = "DATA_READY"
root_dir = Path(os.getcwd())
data_root = root_dir / "data"

# shared log config
logger = logging.getLogger(__name__)
log_template = "[%(asctime)s] %(name)-50s [%(levelname)-7s]  %(message)s"
log_format = logging.Formatter(log_template)
log_file = Path("testdata.log")


@click.group()
@click.pass_context
@click.option(
    "--spaces-key", envvar="SPACES_KEY", help="DigitalOCean Spaces API key",
)
@click.option(
    "--spaces-secret", envvar="SPACES_SECRET", help="DigitalOcean Spaces API secret",
)
@click.option("--verbose", "log_level", flag_value=logging.INFO, help="Increase log level")
@click.option("--debug", "log_level", flag_value=logging.DEBUG, help="Max logging")
@click.option(
    "--datasets",
    "-d",
    type=click.Path(exists=True, dir_okay=False),
    default="datasets.json",
    show_default=True,
    help="JSON file containing datasets and versions",
)
@click.option(
    "--threads",
    type=int,
    default=min(os.cpu_count(), 20),
    show_default=True,
    help="Maximum number of threads to use",
)
@click.option("--config-file", envvar="SPACES_CONFIG", default="/opt/vcpipe-testdata/spaces-config.json", type=click.Path(exists=True, dir_okay=False), help="Config file")
def cli(ctx, **kwargs):
    # init logger
    log_level = kwargs["log_level"] or logging.WARNING
    set_logs(log_level)

    # update DataManager opts
    MGR_OPTS["max_threads"] = kwargs["threads"]
    MGR_OPTS["access_key"] = kwargs["spaces_key"]
    MGR_OPTS["access_secret"] = kwargs["spaces_secret"]
    if kwargs["config_file"]:
        with open(kwargs["config_file"]) as config_file:
            MGR_OPTS.update(json.load(config_file))

    # load datasets info
    with open(kwargs["datasets"]) as datasets_file:
        datasets = json.load(datasets_file)

    # pass on shared data to all subcommands (download, upload, package)
    ctx.ensure_object(dict)
    ctx.obj["datasets"] = datasets


@cli.command(context_settings=CLICK_SETTINGS, help="Download specified test data")
@click.pass_context
@click.option(
    "--data-type",
    multiple=True,
    type=click.Choice(VALID_TYPES),
    default=[VALID_TYPES[0],],
    show_default=True,
    help="Type of data to download",
)
@click.option(
    "--platform",
    multiple=True,
    type=click.Choice(VALID_PLATFORMS),
    default=[VALID_PLATFORMS[0],],
    show_default=True,
    help="Only download data from a specific platform, can be repeated for multiple platforms",
)
@click.option(
    "sample_ids",
    "--sample-id",
    multiple=True,
    metavar="SAMPLE_ID",
    default=["all",],
    help="Only download data from a specific sample ID, can be repeated for multiple samples",
)
def download(ctx, **kwargs):
    mgr = init_mgr()
    kwargs["ctx"] = ctx
    logger.info(
        f"Downloading platform(s) {kwargs['platform']}, sample(s) {kwargs['sample_ids']}, data type(s) {kwargs['data_type']}"
    )
    datasets = get_datasets(**kwargs)

    for platform in datasets:
        for data_type in datasets[platform]:
            for sample, params in datasets[platform][data_type].items():
                mgr.download_package(**params)


@cli.command(
    context_settings=CLICK_SETTINGS, help="Upload specified test data (must be packaged first)"
)
@click.pass_context
@click.option(
    "--platform",
    multiple=True,
    type=click.Choice(VALID_PLATFORMS),
    default=[VALID_PLATFORMS[0]],
    show_default=True,
    help="Only upload data from a specific platform, can be repeated for multiple platforms",
)
@click.option(
    "sample_ids",
    "--sample-id",
    multiple=True,
    metavar="SAMPLE_ID",
    default=["all",],
    help="Only upload data from a specific sample ID, can be repeated for multiple samples",
)
def upload(ctx, **kwargs):
    # always upload samples/analyses together, so doesn't take that option
    mgr = init_mgr()
    kwargs["ctx"] = ctx
    kwargs["data_type"] = VALID_TYPES[0]
    logger.info(f"Uploading platform(s) {kwargs['platform']}, sample(s) {kwargs['sample_ids']}")
    datasets = get_datasets(**kwargs)

    for platform in datasets:
        for data_type in datasets[platform]:
            for sample, params in datasets[platform][data_type].items():
                try:
                    mgr.upload_package(**params)
                except RuntimeError as e:
                    if "Found existing remote files" in str(e):
                        logger.warn(
                            f"Found existing files for {params['path']} version {params['version']} in DigitalOcean, skipping"
                        )
                    else:
                        raise


@cli.command(context_settings=CLICK_SETTINGS, help="Marks new test data as ready for upload")
@click.pass_context
@click.option(
    "--platform",
    multiple=True,
    type=click.Choice(VALID_PLATFORMS),
    default=[VALID_PLATFORMS[0]],
    show_default=True,
    help="Only package data from a specific platform, can be repeated for multiple platforms",
)
@click.option(
    "sample_ids",
    "--sample-id",
    multiple=True,
    metavar="SAMPLE_ID",
    default=["all",],
    help="Only package data from a specific sample ID, can be repeated for multiple samples",
)
def package(ctx, **kwargs):
    kwargs["ctx"] = ctx
    kwargs["data_type"] = VALID_TYPES[0]
    parsed_datasets = get_datasets(**kwargs)

    for platform in parsed_datasets:
        for data_type in parsed_datasets[platform]:
            for sample_id, params in parsed_datasets[platform][data_type].items():
                if not params["path"].exists():
                    logger.error(f"Cannot package {params['path']}: does not exist")
                    continue
                dataset_ready = params["path"] / TOUCHFILE
                dataset_name = f"{params['path']}"

                if dataset_ready.exists():
                    dataset_metadata = load_yaml(dataset_ready.open("rt"), Loader=Loader)
                    if dataset_metadata.get("version", "") == params["version"]:
                        logger.info(f"{dataset_name} already packaged, skipping")
                    else:
                        logger.error(
                            f"Found existing {dataset_name} version {dataset_metadata['version']}, but"
                            f" trying to generate version {params['version']}. Please delete "
                            f"{params['path']} and try again."
                        )
                    continue

                dump_yaml(
                    {"version": params["version"], "timestamp": datetime.datetime.utcnow()},
                    dataset_ready.open("wt"),
                    Dumper=Dumper,
                )
                logger.info(f"Marked {dataset_name} as ready to upload")


@cli.command(
    context_settings=CLICK_SETTINGS, help="List samples available for each selected platform"
)
@click.pass_context
@click.option(
    "platforms",
    "--platform",
    multiple=True,
    type=click.Choice(VALID_PLATFORMS),
    default=[VALID_PLATFORMS[0]],
    show_default=True,
    help="Only list samples from the specified platforms, can be repeated for multiple platforms",
)
@click.option(
    "show_data_types",
    "--show-data-types",
    is_flag=True,
    show_default=True,
    help="List data types available for each sample",
)
def list_samples(ctx, platforms, show_data_types):
    if VALID_PLATFORMS[0] in platforms:
        list_platforms = VALID_PLATFORMS[1:]
    else:
        list_platforms = platforms

    samples = {p: {} for p in list_platforms}
    for platform in ctx.obj["datasets"]:
        if platform not in list_platforms:
            continue

        for sample_id, sample in ctx.obj["datasets"][platform]["samples"].items():
            samples[platform][sample_id] = sample.get("data_types", [])

    for platform_name, platform_samples in samples.items():
        if platform_samples:
            print(f"{platform_name}:")
            for sample_id in sorted(platform_samples):
                sample_str = f"\t{sample_id}"
                if show_data_types:
                    sample_str += ": " + ", ".join(platform_samples[sample_id])
                print(sample_str)
        else:
            print(f"{platform_name}: no samples found")
        print()


def init_mgr():
    return DataManager(**MGR_OPTS)


def set_logs(log_level):
    logger.setLevel(log_level)
    file_handler = logging.FileHandler(log_file, encoding="utf-8")
    file_handler.setLevel(log_level)
    file_handler.setFormatter(log_format)
    logger.addHandler(file_handler)


def get_datasets(ctx, data_type, platform, sample_ids):
    if VALID_PLATFORMS[0] in platform:
        # if `--platform all` is ever used, include everything
        platforms = VALID_PLATFORMS[1:]
    else:
        platforms = platform

    final_opts = dict()
    for dataset_platform_type in platforms:
        # only two data_types, so only need a value instead of list
        if VALID_TYPES[0] in data_type:
            final_opts[dataset_platform_type] = {dt: {} for dt in VALID_TYPES[1:]}
        else:
            final_opts[dataset_platform_type] = {dt: {} for dt in data_type}

        # shortened variable for convenience
        samples = ctx.obj["datasets"][dataset_platform_type]["samples"]
        if sample_ids[0] == "all":
            download_samples = [x for x in samples]
        else:
            sample_pattern = "|".join(sample_ids)
            download_samples = [x for x in samples if re.search(sample_pattern, x)]

        # if no samples in platform.data_type meet requirements, skip it
        if len(download_samples) == 0:
            continue

        # otherwise build out version, path, and name params for each sample that meets criteria
        for sample_id in download_samples:
            for dt in final_opts[dataset_platform_type]:
                if dt not in samples[sample_id]["data_types"]:
                    logger.debug(f"Skipping {dataset_platform_type}/{sample_id}: no {dt} data")
                    continue
                if dataset_platform_type == "goldstandard":
                    # goldstandard has slightly different structure, since it is just reference / analysis data
                    data_dir = data_root / dataset_platform_type / sample_id
                else:
                    data_dir = data_root / dataset_platform_type / dt / sample_id
                final_opts[dataset_platform_type][dt][sample_id] = {
                    "version": samples[sample_id]["version"],
                    "path": data_dir.relative_to(root_dir),
                    "name": f"{dataset_platform_type}/{dt}/{sample_id}",
                }

    return final_opts
